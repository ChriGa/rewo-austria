<?php

/**
 * @copyright	Copyright (C) 2015 Cédric KEIFLIN alias ced1870
 * http://www.joomlack.fr
 * @license		GNU/GPL
 * */
 
 /**
 * Cookies management Javascript code from
 * @subpackage		Modules - mod_jbcookies
 * 
 * @author			JoomBall! Project
 * @link			http://www.joomball.com
 * @copyright		Copyright © 2011-2014 JoomBall! Project. All Rights Reserved.
 * @license			GNU/GPL, http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');
jimport('joomla.plugin.plugin');

class plgSystemCookiesck extends JPlugin {

	function __construct(&$subject, $config) {
		parent :: __construct($subject, $config);
	}

	function onAfterDispatch() {
		global $ckjqueryisloaded;
		$app = JFactory::getApplication();
		$document = JFactory::getDocument();
		$doctype = $document->getType();

		// si pas en frontend, on sort
		if ($app->isAdmin()) {
			return false;
		}

		// si pas HTML, on sort
		if ($doctype !== 'html') {
			return;
		}

		// si en mode imbriqué
		if ($app->input->get('tmpl','') != '') {
			return;
		}

		// load jquery
		$jquerycall = "";
		if (version_compare(JVERSION, '3') >= 1 ) { 
			JHTML::_('jquery.framework', true);
		} else if (! $ckjqueryisloaded) {
			$document->addScript(JUri::base(true) . "/plugins/system/cookiesck/assets/jquery.min.js");
		}

		// get the params from the plugin options
		$plugin = JPluginHelper::getPlugin('system', 'cookiesck');
		$pluginParams = new JRegistry($plugin->params);

		// load the language strings of the plugin
		$this->loadLanguage();

		$lifetime = (int) $pluginParams->get('lifetime', 365);
		// set the cookie from the ajax request on click
		if(isset($_POST['set_cookie'])):
				if($_POST['set_cookie']==1)
					setcookie("cookiesck", "yes", time()+3600*24*$lifetime, "/");
		endif;

		$readmore_link = '';
		$link_rel = '';
		if ($pluginParams->get('linktype', 'article') == 'article') {
			$id = $pluginParams->get('article_readmore');
			// $model->setState('filter.language', $app->getLanguageFilter());
			if ($id) {
				require_once JPATH_SITE.'/components/com_content/helpers/route.php';
				require_once JPATH_SITE.'/components/com_content/helpers/association.php';
				JModelLegacy::addIncludePath(JPATH_SITE.'/components/com_content/models', 'ContentModel');
				$langTag = $app->getLanguage()->getTag();

				$assoc_articles = ContentHelperAssociation ::getAssociations($id);
				if (isset ($assoc_articles[$langTag])) {
					$readmore_link = JRoute::_($assoc_articles[$langTag]);
				} else {
					// Get an instance of the generic article model
					$model = JModelLegacy::getInstance('Article', 'ContentModel', array('ignore_request' => true));
					// Set application parameters in model
					$appParams = JFactory::getApplication()->getParams();
					$model->setState('params', $appParams);
					//	Retrieve Content
					$item = $model->getItem($pluginParams->get('article_readmore'));
					$item->slug = $item->id.':'.$item->alias;
					// $item->catslug = $item->catid.':'.$item->category_alias;
					// get the article link
					$readmore_link = JRoute::_(ContentHelperRoute::getArticleRoute($item->slug, $item->catid));
				}
				if ($link_anchor = $pluginParams->get('link_anchor')) {
					$readmore_link = $readmore_link . '#' . trim($link_anchor, '#');
				}
				$link_rel = $pluginParams->get('link_rel') ? ' rel=\"' . $pluginParams->get('link_rel') . '\"' : '';
			}
		} else {
			$readmore_link = $pluginParams->get('link_readmore');
			if (substr($readmore_link, 0,4) != 'http') {
				$readmore_link = JUri::root(true) . '/' . trim($readmore_link, '/');
			}
		}

		// create the JS to manage the action
		$js = 'jQuery(document).ready(function($){
				$("#cookiesck").remove();
				$("body").append("<div id=\"cookiesck\" />");
				
				$("#cookiesck").append("<span class=\"cookiesck_inner\">'.JText::_('COOKIESCK_INFO').' Weitere Informationen erhalten Sie in unserem <a href=\"/impressum\">Impressum</a> und auf unserer </span>")
					' . ($readmore_link ? '.append("<a href=\"' . $readmore_link . '\" ' . $link_rel . ' target=\"' . ($pluginParams->get('link_target', 'same') == 'new' ? '_blank' : '') . '\" id=\"cookiesck_readmore\">'.JText::_('COOKIESCK_MORE').'</a>")' : '') . '
					.append("<div id=\"cookiesck_accept\">'.JText::_('COOKIESCK_ACCEPT').'</div>")
					.append("<div style=\"clear:both;\"></div>");
			});
			
			jQuery(document).ready(function () { 
		
			function setCookie(c_name,value,exdays)
			{
				var exdate=new Date();
				exdate.setDate(exdate.getDate() + exdays);
				var c_value=escape(value) + ((exdays==null) ? "" : "; expires="+exdate.toUTCString()) + "; path=/";
				document.cookie=c_name + "=" + c_value;
			}

			function readCookie(name) {
				var nameEQ = name + "=";
				var cooks = document.cookie.split(\';\');
				for(var i=0;i < cooks.length;i++) {
					var c = cooks[i];
					while (c.charAt(0)==\' \') c = c.substring(1,c.length);
						if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
					}
				return null;
			}

			var $cookieck = jQuery(\'#cookiesck\');
			var $jb_infoaccept = jQuery(\'#cookiesck_accept\');
			var cookiesck = readCookie(\'cookiesck\');
			$cookieck.hide();
			if(!(cookiesck == "yes")){
			
				// $cookieck.delay(1000).slideDown(\'fast\'); 
				$cookieck.show(); 

				$jb_infoaccept.click(function(){
					setCookie("cookiesck","yes",'.$lifetime.');
					jQuery.post(\''.JUri::current().'\', \'set_cookie=1\', function(){});
					$cookieck.slideUp(\'slow\');
				});
			} 
		});
		';
		$document->addScriptDeclaration($js);

		$where = 'top';
		switch ($pluginParams->get('position', 'absolute')) {
			case 'absolute':
			default:
				$position = 'absolute';
				break;
			case 'fixed':
				$position = 'fixed';
				break;
			case 'relative':
				$position = 'relative';
				break;
			case 'bottom':
				$position = 'fixed';
				$where = 'bottom';
				break;
		}
		// add styling
		$css = "
			#cookiesck {
				position:" . $position . ";
				left:0;
				right: 0;
				" . $where . ": 0;
				z-index: 99;
				min-height: 30px;
				color: " . $pluginParams->get('text_color', '#eee') . ";
				background: " . $this->hex2RGB($pluginParams->get('background_color', '#000000'), $pluginParams->get('background_opacity', '0.5')) . ";
				box-shadow: #000 0 0 2px;
				text-align: center;
				font-size: 14px;
				line-height: 14px;
			}
			#cookiesck .cookiesck_inner {
				padding: 10px 0;
				display: inline-block;
			}
			#cookiesck_readmore {
				float:right;
				padding:10px;
				border-radius: 3px;
			}
			#cookiesck_accept{
				float:right;
				padding:10px;
				margin: 1px;
				border-radius: 3px;
				background: #000;
				cursor: pointer;
				-webkit-transition: all 0.2s;
				transition: all 0.2s;
				border: 1px solid #404040;
			}
			#cookiesck_accept:hover{
				font-size: 120%;
			}
		";
		$document->addStyleDeclaration($css);
	}
	
	/**
	 * Convert a hexa decimal color code to its RGB equivalent
	 *
	 * @param string $hexStr (hexadecimal color value)
	 * @param boolean $returnAsString (if set true, returns the value separated by the separator character. Otherwise returns associative array)
	 * @param string $seperator (to separate RGB values. Applicable only if second parameter is true.)
	 * @return array or string (depending on second parameter. Returns False if invalid hex color value)
	 */
	function hex2RGB($hexStr, $opacity) {
		if ($opacity > 1) $opacity = $opacity/100;
		$hexStr = preg_replace("/[^0-9A-Fa-f]/", '', $hexStr); // Gets a proper hex string
		$rgbArray = array();
		if (strlen($hexStr) == 6) { //If a proper hex code, convert using bitwise operation. No overhead... faster
			$colorVal = hexdec($hexStr);
			$rgbArray['red'] = 0xFF & ($colorVal >> 0x10);
			$rgbArray['green'] = 0xFF & ($colorVal >> 0x8);
			$rgbArray['blue'] = 0xFF & $colorVal;
		} elseif (strlen($hexStr) == 3) { //if shorthand notation, need some string manipulations
			$rgbArray['red'] = hexdec(str_repeat(substr($hexStr, 0, 1), 2));
			$rgbArray['green'] = hexdec(str_repeat(substr($hexStr, 1, 1), 2));
			$rgbArray['blue'] = hexdec(str_repeat(substr($hexStr, 2, 1), 2));
		} else {
			return false; //Invalid hex color code
		}
		$rgbacolor = "rgba(" . $rgbArray['red'] . "," . $rgbArray['green'] . "," . $rgbArray['blue'] . "," . $opacity . ")";

		return $rgbacolor;
	}

}