<?php 

/**
 * @package 	description.php
 * @author		Aplikko
 * @email		contact@aplikko.com
 * @website		http://aplikko.com
 * @copyright	Copyright (C) 2014 Aplikko.com. All rights reserved.
 * @license		http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
**/

// no direct access
defined('_JEXEC') or die('Restricted access');

jimport('joomla.form.formfield');

class JFormFieldDescription extends JFormField {
	protected $type = 'Description';

	/**
	* Method to get a form field markup for the field input.
	*/
	protected function getInput() {
	
	//$doc = JFactory::getDocument();
	$srcpath = JURI::root(true).'/modules/'.basename(dirname(__DIR__));
	$thumbName = str_replace('mod_', '', basename(dirname(__DIR__)));
	$moduleName = str_replace('_',' ',str_replace('mod_', '', basename(dirname(__DIR__))));
	$moduleName = ucwords($moduleName);
    $moduleName[1] = strtoupper($moduleName[1]);


	return '
    <div class="intro">
		<h2>'.$moduleName.' Module<span class="pro">PRO</span><span class="version">ver. 3.3</span></h2>
		<p>'.$moduleName.' Module is a simple, easy to use and easy to customize contact module for your joomla site. This module displays a responsive, bootstrapped, quick <b>contact form</b> with data validation on the fly, including ajax support for success message and a simple captcha that will protect you from bots and spams. Captcha can be deactivated, if you may not need it. Fields are validated via Javascript and if there are no errors e-mails are sent with an Ajaxed interface. It is designed for easy usage and to fit any design. For best results, we recommend to choose SMTP as a default mailer in Global Configuration\'s (Mail Settings).</p>
		<h4 class="features">Features:</h4>
		<ul>
			<li><i class="fa fa-check-square-o"></i><b>HTML5 & CSS3</b>.</li>
			<li><i class="fa fa-check-square-o"></i><b>Responsive!</b> It works on all devices.</li>
			<li><i style="color:#924189;font-size:14px;margin:3px 3px 0 -16px;" class="icomoon-bootstrap"></i>Adapated for <b>Bootstrap</b>.</li>
			<li><i class="fa fa-check-square-o"></i><b>Validation</b> on the fly.</li>
			<li><i class="fa fa-check-square-o"></i><b>Ajax success message</b> with WYSIWYG html editor in admin.</li>
			<li><i class="fa fa-check-square-o"></i><b>Cross-Browser</b> support. Works great in all the modern browsers.</li>		
			<li><i class="fa fa-check-square-o"></i><b>Captcha</b> against spam bots is included.</li>
		</ul>
		<div class="license">
			<img class="img-rounded" style="width:110px;height:auto;float:left;margin:0 20px 0 20px;" src="'.$srcpath.'/admin/images/'.$thumbName.'.png" alt="" />
			<span class="title">'.$moduleName.' Module<span class="pro">PRO</span><small style="color:#fff;">ver. 3.3</small><br /><br /></span>
			<div class="getmore">Get more extensions from Aplikko <a class="hasTooltip" title="Aplikko Extensions Page" href="http://www.aplikko.com/joomla-extensions" target="_blank">extensions</a> page.<br />Powerfully simple! From <a href="http://www.aplikko.com" target="_blank">Aplikko.com</a>.</div>
		</div>  
    </div>';	
	}
	
	
	/**
	 * Method to get a control group with label and input.
	 * @since   3.2
	 */
	public function renderField($options = array()) {
	  return $this->getInput();
 	}
	
}