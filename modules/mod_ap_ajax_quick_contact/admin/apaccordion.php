<?php
/**
 * @package 	apaccordion.php
 * @author		Aplikko
 * @email		contact@aplikko.com
 * @website		http://aplikko.com
 * @copyright	Copyright (C) 2014 Aplikko.com. All rights reserved.
 * @license		http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
**/

// no direct access
defined('_JEXEC') or die('Restricted access');

jimport('joomla.form.formfield');



class JFormFieldApaccordion extends JFormField {
	protected $type = 'Apaccordion';
	

	/** Nothing to show as label */
	protected function getLabel() { return ''; }
	
	/**
	* Method to get a form field markup for the field input.
	* @param   string  $name   The name of the form field.
	* @param   string  $group  The optional dot-separated form group path on which to find the field.
	* @param   mixed   $value  The optional value to use as the default for the field.
	*/
	protected function getInput() {

	if ($this->element['name'] == 'email_info') { 
		$email_info = '
		<div id="'.$this->element['name'].'" class="apaccordion">
			<a class="accordion-toggle sub-heading" href="#collapse'.$this->element['name'].'" data-parent="#'.$this->element['name'].'" data-toggle="collapse"> 
				<div class="accordion-toggle">
					<span class="sub-heading-info">
					<i class="fa fa-question-circle"></i>
					Email
					</span>
					<span class="toggle-icon"><i class="fa fa-plus-square '.$this->element['name'].'"></i></span>
				</div>
			</a>
			<div id="collapse'.$this->element['name'].'" class="accordion-body collapse '.$this->element['name'].'">
				<div class="sub-heading-infotext accordion-toggle" data-toggle="collapse">
					INFO: Make sure you put "Recipient email address" here. To this email address sent emails will come.
				</div>
			</div>
		</div>
			<script type="text/javascript">
			   jQuery(document).ready(function(){			
				// accordion toggle plus/minus
				jQuery("#'.$this->element['name'].'").on("show", function () {
				  jQuery("#'.$this->element['name'].' .accordion-toggle").find(".fa-plus-square.'.$this->element['name'].'").toggleClass("fa-plus-square fa-minus-square");
				});
				jQuery("#'.$this->element['name'].'").on("hidden", function () {
				  jQuery("#'.$this->element['name'].' .accordion-toggle").find(".fa-minus-square.'.$this->element['name'].'").toggleClass("fa-minus-square fa-plus-square");
				});
			   });	
			</script>
			';
		return $email_info;
		}

	if ($this->element['name'] == 'success_message') { 
		$success_message = '
		<div id="'.$this->element['name'].'" class="apaccordion">
			<a class="accordion-toggle sub-heading" href="#collapse'.$this->element['name'].'" data-parent="#'.$this->element['name'].'" data-toggle="collapse"> 
				<div class="accordion-toggle">
					<span class="sub-heading-info">
					<i class="fa fa-question-circle"></i>
					Success Message
					</span>
					<span class="toggle-icon"><i class="fa fa-plus-square '.$this->element['name'].'"></i></span>
				</div>
			</a>
			<div id="collapse'.$this->element['name'].'" class="accordion-body collapse '.$this->element['name'].'">
				<div class="sub-heading-infotext accordion-toggle" data-toggle="collapse">
					INFO: You can enter here a custom "Success" message with html code. You can use your WYSIWYG html editor. This message will appear with ajax, after submitted email.
				</div>
			</div>
		</div>
			<script type="text/javascript">
			   jQuery(document).ready(function(){			
				// accordion toggle plus/minus
				jQuery("#'.$this->element['name'].'").on("show", function () {
				  jQuery("#'.$this->element['name'].' .accordion-toggle").find(".fa-plus-square.'.$this->element['name'].'").toggleClass("fa-plus-square fa-minus-square");
				});
				jQuery("#'.$this->element['name'].'").on("hidden", function () {
				  jQuery("#'.$this->element['name'].' .accordion-toggle").find(".fa-minus-square.'.$this->element['name'].'").toggleClass("fa-minus-square fa-plus-square");
				});
			   });	
			</script>
			';
		return $success_message;
		}

	}

	/**
	 * Method to get a control group with label and input.
	 * @since   3.2
	 */
	public function renderField($options = array()) {
	  return $this->getInput();
 	}
	
	
}