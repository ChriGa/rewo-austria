<?php
/**
 * @package 	apemail.php
 * @author		Aplikko
 * @email		contact@aplikko.com
 * @website		http://aplikko.com
 * @copyright	Copyright (C) 2014 Aplikko.com. All rights reserved.
 * @license		http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
**/

// no direct access
defined('_JEXEC') or die('Restricted access');

JFormHelper::loadFieldClass('text');

class JFormFieldApemail extends JFormFieldText {
	
	protected $type = 'Apemail';

        protected function getInput() {

        //$output = NULL;
		// Translate placeholder text
		$hint = $this->translateHint ? JText::_($this->hint) : $this->hint;
		
		// Initialize some field attributes.
		$size         = !empty($this->size) ? ' size="' . $this->size . '"' : '';
		$maxLength    = !empty($this->maxLength) ? ' maxlength="' . $this->maxLength . '"' : '';
		$class        = !empty($this->class) ? ' class="validate-email ' . $this->class . '"' : ' class="validate-email"';
		$readonly     = $this->readonly ? ' readonly' : '';
		$disabled     = $this->disabled ? ' disabled' : '';
		$required     = $this->required ? ' required aria-required="true"' : '';
		$hint         = $hint ? ' placeholder="' . $hint . '"' : '';
		$autocomplete = !$this->autocomplete ? ' autocomplete="off"' : ' autocomplete="' . $this->autocomplete . '"';
		$autocomplete = $autocomplete == ' autocomplete="on"' ? '' : $autocomplete;
		$autofocus    = $this->autofocus ? ' autofocus' : '';
		$multiple     = $this->multiple ? ' multiple' : '';
		$spellcheck   = $this->spellcheck ? '' : ' spellcheck="false"';
		
		$prepend    = ($this->element['prepend'] != NULL) ? '<span class="add-on">'. JText::_($this->element['prepend']). '</span>' : '';

		$append   = ($this->element['append'] != NULL) ? '<span class="add-on" data-trigger="hover" data-toggle="popover" data-placement="right" data-content="'.JText::_($this->element['data-content']).'" title="'.JText::_($this->element['title']).'">'.JText::_($this->element['append']).'</span>' : '';

		$wrapstart  = '<div id="field-wrap">';
		$wrapend    = '</div>';
		
		// Initialize JavaScript field attributes.
		$onchange = $this->onchange ? ' onchange="' . $this->onchange . '"' : '';

		// Including fallback code for HTML5 non supported browsers.
		JHtml::_('jquery.framework');
		JHtml::_('script', 'system/html5fallback.js', false, true);

		
		return $wrapstart . $prepend . '<input type="email" name="' . $this->name . '"' . $class . ' id="'.$this->id.'"'
		. ' value="'.htmlspecialchars(JStringPunycode::emailToUTF8($this->value), ENT_COMPAT, 'UTF-8').'"'
		. $spellcheck . $size . $disabled . $readonly . $onchange . $autocomplete . $multiple . $maxLength . $hint . $required . $autofocus . '/>' . $append. $wrapend;

	}

}
