<?php 

/**
 * @package 	mod_ap_ajax_quick_contact.php - AP Ajax Quick Contact Module
 * @version		3.3
 * @author		Aplikko
 * @email		contact@aplikko.com
 * @website		http://aplikko.com
 * @copyright	Copyright (C) 2014 Aplikko.com. All rights reserved.
 * @license		http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
**/

//no direct access
defined('_JEXEC') or die;
ini_set('display_errors',0);

// Global variables  
$doc = JFactory::getDocument();

$layout = $params->get('js', 'jqscript');
// Path assignments
$path = str_replace("&", "",$path);
$ibase = JURI::base();
if(substr($ibase, -1)=="/") { $ibase = substr($ibase, 0, -1); }
$modURL = 'modules/mod_ap_ajax_quick_contact';

// Load css
$doc->addStyleSheet($modURL.'/assets/css/style.css');
$doc->addStyleSheet('//netdna.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css');// font awesome url

// Parameters
$uniqid = $module->id;
$form_width = $params->get('form_width');
$name = $params->get('name','Name');
$email = $params->get('email','Email');
$message = $params->get('message','Message');
$captcha_label = $params->get('captcha_label','0');
$captcha = $params->get('captcha','Captcha');
$ap_captchaError = $params->get('ap_captchaError','Please enter the correct Captcha!'); 
$submit = $params->get('submit','Send');
$ap_error_email_not_set = $params->get('ap_error_email_not_set');
// $ap_send_message params with decoded hmtl characters from editor in admin (Ajax message when email has been sent)
$ap_send_message = $params->get('ap_send_message');

// remove comments from sent message (if any) */
$ap_send_message = preg_replace('!/\*[^*]*\*+([^/][^*]*\*+)*/!', '',$ap_send_message);
// remove tabs, spaces, new lines, etc. */ 
$ap_send_message = str_replace(array("\r\n","\r","\n","\t",'  ','    ','    '),'',$ap_send_message);
// replace apostrophe to avoid problems */ 
$ap_send_message = str_replace("'","&#39;", $ap_send_message);

$ap_error_name = $params->get('ap_error_name','Please enter your Name!');
$ap_error_email = $params->get('ap_error_email','Please enter your Email address!');
$ap_error_field = $params->get('ap_error_field');
$ap_script_required = $params->get('ap_script_required');
$ap_script_email = $params->get('ap_script_email');
$subject = $params->get('subject');
$recipient = $params->get('recipient','');
$moduleclass_sfx = $params->get('moduleclass_sfx');
$recipientClean = str_replace("@", " (at) ", $recipient);
$dsgvo_text = 'Ich stimme zu, dass meine Angaben aus dem Kontaktformular zur Beantwortung meiner Anfrage erhoben und verarbeitet werden.
Hinweis: Sie können Ihre Einwilligung jederzeit für die Zukunft per E-Mail an <span style="font-weight: bold; color: #000000;">' .$recipientClean. '</span> widerrufen.';

if(isset($_POST['submitted'])) {
	// require a name from user
	if(trim($_POST['ap_name']) === '') {
		$_POST['ap_name'] = "Kein Name angegeben";
/*		$nameError =  ''; 
		$hasError = true;*/
	} else {
		$name = trim($_POST['ap_name']);
	}
	// need valid email
	if(trim($_POST['ap_email']) === '')  {
		$emailError = 'Please enter your Email!';
		$hasError = true;
	} else if (!preg_match("/^[[:alnum:]][a-z0-9_.-]*@[a-z0-9.-]+\.[a-z]{2,4}$/i", trim($_POST['ap_email']))) {
		$emailError = 'Invalid email address.';
		$hasError = true;
	} 
	// we need at least some content
	if(trim($_POST['ap_message']) === '') {
		$messageError = 'Please enter your Message!';
		$hasError = true;
	} else {
		if(function_exists('stripslashes')) {
			$message = stripslashes(trim($_POST['ap_message']));
		} else {
			$message = trim($_POST['ap_message']);
		}
	}
	// require a valid captcha
	if ($captcha_label == "1") {
	if(trim($_POST['ap_captcha']) != $_SESSION['expect']) {
		$captchaError = $params->get('ap_captchaError','Please enter correct Captcha!');
		$hasError = true;
	} else {
		unset ($_SESSION['n1']);
		unset ($_SESSION['n2']);
		unset ($_SESSION['expect']);
		$captcha = trim($_POST['ap_captcha']);
	}}
	//CG 089: ist DSGVO Checkbox gecheckt? wird hier eigentlich nicht benötigt! wg "required" im field in der view!
		$dsgvoErrorText = "Bitte bestätigen Sie den Hinweis bzgl Datenschutz(DSGVO)";
		$dsgvoError = false;
	if(!isset($_POST['dsgvo_checkbox'])) {
		$dsgvoError = true;
		$hasError = true;
	} else {
		$dsgvoError = false;
	}
	// ...


	// let's email:
	if(!isset($hasError)) {
		$mail =& JFactory::getMailer();		
		$config =& JFactory::getConfig();
		$sender = array($_POST['ap_email'],$_POST['ap_name'] );
		$mail->setSender($sender);
		$mail->setSubject($subject);
		$mail->addRecipient($recipient);
	
		$body = "<div style=\"line-height:22px;font-size:110%;\">Subject: ".$subject."<br/>";
		$body.= "Name: ".$_POST['ap_name']."<br/>";
		$body.= "Email: ".$_POST['ap_email']."<br/><br/></div>";
		$body.= "<div style=\"padding:15px 0;line-height:22px;width:100%;display:block;font-size:110%;border-top:1px solid #ccc;\"><strong>Message:</strong> ".$_POST['ap_message']."<br/><div/>";
	
		$mail->setBody($body);
		$mail->IsHTML(true);
		$send =& $mail->Send();
		$emailSent = true;
	}
}
	if ($captcha_label == "1") {
		$_SESSION['n1'] = rand(1,10);
		$_SESSION['n2'] = rand(1,10);
		$_SESSION['expect'] = $_SESSION['n1']+$_SESSION['n2'];
	}

require JModuleHelper::getLayoutPath('mod_ap_ajax_quick_contact', $params->get('layout', 'default'));
require __DIR__ . '/assets/js/jqscript.php'; 
?>