<?php
/**
 * @author   	089webdesgin.de
 * @copyright   Copyright (C) 2015 089webdesgin.de. All rights reserved.
 * @URL 		https://089webdesgin.de/
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
 
defined('_JEXEC') or die;
?>
<?php if(!$detect->isMobile()) : ?>    
  <div class="menuWrapper fullwidth">
      <div class="vcard">
          <h3 class="vcardHeader">Service &amp; Info</h3>
          <p class="tel "><a class="" href="tel:+4319112668">Tel: 01-911 26 68</a></p>
          <p class="adr"><span class="street-address">Semperstraße 60</span><br />
            <span class="postal-code">1180 </span><span class="region">Wien Österreich</span>
          </p>
      </div>
    <nav class="navbar-wrapper">
        <div class="logo-small">
          <a href="/">
              <img class="logoImg" src="/images/rewo-logo-klein.png" alt="Logo der REWO GmbH" />
          </a>
        </div>
        <h3 class="logoHeader"><span>R</span>ewo <span>S</span>icherheitstechnik GmbH</h3>
        <div class="navbar">
          <div class="navbar-inner">
            <button type="button" class="btn btn-navbar collapsed" >
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>                                   
  		  <?php if ($this->countModules('menu')) : ?>
        			<div class="nav-collapse collapse "  role="navigation">
        				<jdoc:include type="modules" name="menu" style="custom" />
        			</div>
         <?php endif; ?>
          </div>
        </div>      
    </nav>
 
<script type="text/javascript">
  jQuery(document).ready(function() {
      jQuery('.btn-navbar').on("click", function() {
          jQuery('.nav-collapse').toggleClass('in');
      });
  })
</script>  
  <?php if($this->countModules('suche')) : ?>
    <div id="suche">
        <div id="morphsearch" class="morphsearch">
            <jdoc:include type="modules" name="suche" sytle="custom"/>
            <div class="morphsearch-content row-fluid">
                <div class="module-column span6">
                    <jdoc:include type="modules" name="favSuche" style="custom" />  
                </div>
                <div class="module-column span6">
                    <jdoc:include type="modules" name="favSuche2" style="custom" />  
                </div>
            </div>
        </div>
        <div class="overlay"></div>    
    </div>
  <?php endif; ?>
</div> 
<?php else : ?>
            <div id="mobileHeader" class="menuWrapper fullwidth">
                <div class="figHolder mobile">
                  <img alt="figur hr rewo" src="/images/figur-hr-mobile-rewo.png" />
                </div>
                  <div class="lgHeaderWrapper">
                      <h3><span>R</span>ewo <span>S</span>icherheitstechnik GmbH</h3>
                  </div>
                <div class="logo-small">
                    <a href="/" title="REWO Startseite">
                      <img class="logoImg" src="/images/rewo-logo-klein.png" alt="Logo der REWO GmbH" />
                    </a>
                </div>
                <div class="vcard">
                    <h3 class="vcardHeader">Service &amp; Info</h3>
                      <p class="tel "><a class="" href="tel:+4319112668">Tel: 01-911 26 68</a></p>
                      <p class="adr"><span class="street-address">Semperstraße 60</span><br />
                        <span class="postal-code">1180 </span><span class="region">Wien Österreich</span>
                      </p>
                </div>                 
                <div class="clr"></div> 
                <div class="rewoAdresse">
                    <p><span>Semperstraße 60</span><br />
                      <span>1180 </span><span class="region">Wien Österreich</span>
                    </p>
                </div>                 
                <div class="mobileMenuWrapper">
                    <div id="suche">
                        <div id="morphsearch" class="morphsearch">
                            <jdoc:include type="modules" name="suche" sytle="custom"/>
                            <div class="morphsearch-content row-fluid">
                                <div class="module-column span6">
                                    <jdoc:include type="modules" name="favSuche" style="custom" />  
                                </div>
                                <div class="module-column span6">
                                    <jdoc:include type="modules" name="favSuche2" style="custom" />  
                                </div>
                            </div>
                        </div>
                        <div class="overlay"></div>    
                    </div>                
                    <div class="mm-btn">
                        <a href="#menu" class="btn-menu">
                          <p class="btnDesc">Men&uuml;</p>
                            <div id="toggle" class="button_container">
                                <span class="top"></span>
                                <span class="middle"></span>
                                <span class="bottom"></span>
                            </div>
                        </a>
                    </div>
                </div>
                <jdoc:include type="modules" name="menuMobile" style="custom" />
            </div>
<script type="text/javascript">

    var $menu = jQuery('#menu'); //mmenu
    var $btnMenu = jQuery('.btn-menu');
        $menu.mmenu({
           "extensions": [
              "effect-menu-zoom",
              "effect-panels-zoom",
              "pagedim-black",
              "pageshadow"
           ],
           "counters": true,
           navbar: {
                 title: "REWO Menü"        
           },
           "offCanvas": {
              "position": "right"
           }
        });
      $menu.find( ".mm-next" ).addClass("mm-fullsubopen");

</script>            
<?php endif; ?>  
