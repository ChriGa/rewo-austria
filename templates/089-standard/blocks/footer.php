<?php
/**
 * @author   	089webdesgin.de
 * @copyright   Copyright (C) 2015 089webdesgin.de. All rights reserved.
 * @URL 		https://089webdesgin.de/
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
?>
<footer class="footer">
	<div class="footer-wrap">				
		<div class="innerwidth">
			
		</div>	
	</div>
</footer>
<div id="copyright" class="innerwidth"><p>&copy; <?php print date("Y");?> REWO Sicherheitstechnik GmbH | <a class="imprLink" href="/impressum" title="Impressum REWO GmbH">Impressum</a> | <a class="imprLink" href="/datenschutz" title="Datenschutz REWO GmbH">Datenschutz</a></p>
</div>