<?php
/**
 * @author   	089webdesgin.de
 * @copyright   Copyright (C) 2015 089webdesgin.de. All rights reserved.
 * @URL 		https://089webdesgin.de/
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
 
defined('_JEXEC') or die;
?>
<?php if(!$detect->isMobile() || $detect->isTablet() ) : ?>
	<div class="clear-slider">
		<?php if ($menu->getActive() == $menu->getDefault()) : ?>
			<?php /*<div class="extLogoHolder">
				<img class="jabLogo" src="images/rewo-distributor-2017.jpg" alt="JABLOTRON Zertifikat" />
			</div>	*/ ?>
		<?php endif; ?>
			<?php if ($this->countModules('slider')) : ?>
				<div class="clear-slider-wrap" id="sliderWrapper">
					<jdoc:include type="modules" name="slider" style="none" />	
				</div>
			<?php endif; ?>
	</div>
<?php endif; ?>
<div id="descHeader">
	<div class="innerwidth">
		<h3>Ihr Fachhandelspartner für Sicherheitstechnik und Industriefunktechnik</h3>
	</div>
</div>			
