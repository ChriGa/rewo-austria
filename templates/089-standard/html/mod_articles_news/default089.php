<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_articles_news
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
?>
<?php //preprint($list); ?>

<div id="myCarousel" class="carousel slide newsflash<?php echo $moduleclass_sfx; ?>">
<ol class="carousel-indicators">
	<?php 
		$x = 0;
		while ($list[$x]) {
			print '<li data-target="#myCarousel" data-slide-to="'.$x.'" class=""></li>';
			$x++;
		}
	?>
</ol>	
	<div class="carousel-inner">
		<?php foreach ($list as $item) : ?>
			<?php require JModuleHelper::getLayoutPath('mod_articles_news', '_item'); ?>
		<?php endforeach; ?>
	</div>
<?php /*	
	<a class="carousel-control left" href="#myCarousel" data-slide="prev">&lsaquo;</a>
	<a class="carousel-control right" href="#myCarousel" data-slide="next">&rsaquo;</a>	
	*/ ?>
</div>

<script type="text/javascript">
	jQuery(function(){
		jQuery('.carousel-inner div:first, ol.carousel-indicators li:first').addClass('active');

		jQuery('.carousel').carousel({
			  interval: 4000
			});		
	});
</script>